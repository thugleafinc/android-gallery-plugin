package com.wordcore.androidgalleryplugin;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

public class BitmapUtils {

    public static final String TAG = BitmapUtils.class.getSimpleName();

    private static String[] okMimeTypes =  new String[] {"image/jpg", "image/png", "image/gif","image/jpeg"};

    public static Bitmap decodeStreamFromUri(Context context, Uri uri, int requiredDimension)
    {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getInputStreamFromUri(context, uri), null, options);

        if (isImageValid(context, options, requiredDimension)){
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, requiredDimension, requiredDimension);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(getInputStreamFromUri(context, uri), null, options);
        }
        return null;
    }

    public static InputStream getInputStreamFromUri(Context context, Uri uri)
    {
        try {
            return context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static Boolean isImageValid(Context context, BitmapFactory.Options options, int requiredDimension){
        Boolean isImage = false;
        int imageWidth = options.outWidth;
        int imageHeight = options.outHeight;
        String imageMimeType = options.outMimeType;
        for (String mimeType : okMimeTypes) {
            if (!imageMimeType.toLowerCase().equals(mimeType)){
                isImage = true;
                break;
            }
        }
        if (!isImage){
            Log.d(TAG,"Non valid image.");
            Toast.makeText(context, "Selected file does not appear to be a valid image.", Toast.LENGTH_LONG).show();
            return false;
        }

        if (imageHeight <= requiredDimension || imageWidth <= requiredDimension){
            Log.d(TAG,"Image dimensions are small.");
            Toast.makeText(context,"Selected image is too small. Must be bigger than "+requiredDimension+"x"+requiredDimension, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    public static Bitmap centerCropBitmap(Bitmap source){
        try{
            Boolean dimensionTest = source.getWidth() >= source.getHeight();
            Bitmap destination =  Bitmap.createBitmap(
                    source,
                    (dimensionTest)? source.getWidth()/2 - source.getHeight()/2 : 0,
                    (dimensionTest)? 0 : source.getHeight()/2 - source.getWidth()/2,
                    (dimensionTest)? source.getHeight(): source.getWidth(),
                    (dimensionTest)? source.getHeight(): source.getWidth()
            );
            return destination;

        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap rescaleBitmap(Bitmap source, int requiredDimension)
    {
        try {
            return Bitmap.createScaledBitmap(source, requiredDimension, requiredDimension, false);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap rotateBitmap(Bitmap source, int rotation)
    {
        try{
            if (rotation != 0){
                Matrix matrix = new Matrix();
                matrix.postRotate(rotation);
                source = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                        source.getHeight(), matrix, true);
            }
            return source;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void saveBitmap(Bitmap bitmap,File file)
    {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
