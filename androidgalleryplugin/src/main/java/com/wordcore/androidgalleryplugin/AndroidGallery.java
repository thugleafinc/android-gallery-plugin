package com.wordcore.androidgalleryplugin;

import android.app.Activity;
import android.app.Fragment;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.unity3d.player.UnityPlayer;
import java.io.File;

public class AndroidGallery extends Fragment {

    public static final String TAG = AndroidGallery.class.getSimpleName();
    public static final int GALLERY_PICK_REQUEST = 1001;
    public static final int CAPTURE_CAMERA_REQUEST = 1002;
    private static final String CALLBACK_METHOD_NAME = "IntentFinishedWithResult";
    private Uri cachedImageUri;


    public static AndroidGallery instance;
    private String gameObjectName;
    private int imageDimension;
    private static Activity unityActivity;


    public static void start(String gameObjectName, int imageDimension)
    {
        unityActivity = UnityPlayer.currentActivity;
        instance = new AndroidGallery();
        instance.gameObjectName = gameObjectName;
        instance.imageDimension = imageDimension;
        unityActivity.getFragmentManager().beginTransaction().add(instance, AndroidGallery.TAG).commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void pickImageFromGallery(String playerId)
    {
        cachedImageUri = Uri.fromFile(new File(unityActivity.getApplication().getExternalCacheDir(), playerId));
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivityForResult(intent, GALLERY_PICK_REQUEST);
    }

    public void captureImageWithCamera(String playerId)
    {
        Long tsLong = System.currentTimeMillis()/1000;
        String filename = playerId + '_' + tsLong.toString();
        cachedImageUri = Uri.fromFile(new File(UnityPlayer.currentActivity.getApplication().getExternalCacheDir(), filename));
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, cachedImageUri);
        startActivityForResult(intent, CAPTURE_CAMERA_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode) {
            case GALLERY_PICK_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Bitmap bitmap = null;
                    Uri selectedImageUri = data.getData();
                    bitmap = BitmapUtils.decodeStreamFromUri(unityActivity.getBaseContext(), selectedImageUri, imageDimension);
                    if (bitmap == null){
                        Log.d(TAG,"Decoding Error A.");
                        Toast.makeText(unityActivity.getBaseContext(),"Error while decoding file", Toast.LENGTH_LONG).show();
                        break;
                    }
                    bitmap = BitmapUtils.centerCropBitmap(bitmap);
                    bitmap = BitmapUtils.rescaleBitmap(bitmap, imageDimension);
                    BitmapUtils.saveBitmap(bitmap, new File(cachedImageUri.getPath()));
                    sendUnityMessage(CALLBACK_METHOD_NAME, cachedImageUri.getPath(), false);
                }
                break;
            case CAPTURE_CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Bitmap bitmap = null;
                    File f = new File(getRealPathFromURI(cachedImageUri));
                    if (!f.exists()) {
                        Log.d(TAG,"Missing File.");
                        Toast.makeText(unityActivity.getBaseContext(),"Error while capturing image", Toast.LENGTH_LONG).show();
                        return;
                    }
                    try {
                        int rotation = 0;
                        try {
                            ExifInterface exif = new ExifInterface(f.getAbsolutePath());
                            int orientation = exif.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    rotation = 270;
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    rotation = 180;
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    rotation = 90;
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        bitmap = BitmapUtils.decodeStreamFromUri(unityActivity.getBaseContext(), cachedImageUri, imageDimension);
                        if (bitmap == null){
                            Log.d(TAG,"Decoding Error B.");
                            Toast.makeText(unityActivity.getBaseContext(),"Error while decoding file", Toast.LENGTH_LONG).show();
                            break;
                        }
                        bitmap = BitmapUtils.centerCropBitmap(bitmap);
                        bitmap = BitmapUtils.rescaleBitmap(bitmap, imageDimension);
                        bitmap = BitmapUtils.rotateBitmap(bitmap, rotation);
                        BitmapUtils.saveBitmap(bitmap, f);
                        sendUnityMessage(CALLBACK_METHOD_NAME, cachedImageUri.getPath(), true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private void sendUnityMessage(String methodName, String path, boolean deleteAfterUpload)
    {
        StringBuilder args = new StringBuilder();
        args.append(path);
        args.append('|');
        args.append(String.valueOf(deleteAfterUpload));

        Log.d(TAG, args.toString());
        UnityPlayer.UnitySendMessage(gameObjectName, methodName, args.toString());
    }

    private String getRealPathFromURI(Uri uri)
    {
        String result;
        Cursor cursor = unityActivity.getBaseContext().getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


}
